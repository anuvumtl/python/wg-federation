import pytest
from mockito import mock, unstub, verify, when, ANY, contains, arg_that

from unit.wg_federation import hq_state, wireguard_configuration_valid1, wireguard_configuration_valid2, \
    wireguard_configuration_valid3
from wg_federation.data.input.command_line.secret_retreival_method import SecretRetrievalMethod
from wg_federation.data.state.federation import Federation
from wg_federation.data.state.hq_state import HQState
from wg_federation.data.state.wireguard_configuration import WireguardConfiguration
from wg_federation.event.hq.hq_event import HQEvent
from wg_federation.exception.user.data.state_signature_cannot_be_verified import StateNotBootstrapped
from wg_federation.state.manager.state_data_manager import StateDataManager


class TestStateDataManager:
    """ Test StateDataManager class """

    _file = None
    _lock = None
    _user_input = None

    _configuration_location_finder = None
    _configuration_loader = None
    _configuration_saver = None
    _configuration_locker = None
    _wireguard_key_generator = None
    _event_dispatcher = None
    _logger = None

    _subject: StateDataManager = None

    @pytest.fixture(autouse=True)
    def run_around_tests(self):
        """ Resets mock between tests """
        unstub()
        self.init()

    def init(self):
        """ Constructor """

        self._user_input = mock({
            'private_key_retrieval_method': SecretRetrievalMethod.WG_FEDERATION_COMMAND,
            'root_passphrase_command': 'example command',
        })

        self._lock = mock()
        # pylint: disable=unnecessary-dunder-call
        when(self._lock).__enter__(...).thenReturn(self._file)
        when(self._lock).__exit__(...).thenReturn(self._file)

        self._configuration_location_finder = mock()
        when(self._configuration_location_finder).state().thenReturn('state')
        when(self._configuration_location_finder).interfaces_directory().thenReturn('/tmp/interfaces')
        when(self._configuration_location_finder).phone_lines_directory().thenReturn('/tmp/phone_line')
        when(self._configuration_location_finder).forums_directory().thenReturn('/tmp/forums')

        self._configuration_loader = mock()

        when(self._configuration_loader).load_if_exists(self._file).thenReturn(hq_state().dict(exclude_defaults=True))
        self._configuration_saver = mock()

        self._configuration_locker = mock()
        when(self._configuration_locker).lock_exclusively('state').thenReturn(self._lock)
        when(self._configuration_locker).lock_shared('state').thenReturn(self._lock)

        self._wireguard_key_generator = mock()
        when(self._wireguard_key_generator).generate_key_pairs().thenReturn(
            ('TEJkUEP2p2RxXlAKshLgZLwnIAAOhFzNeJhv6wvSxkY=', 'pifGr/4QrabUT9HvzMhYoqTZiZAF1R2kB3d5TU1jLCw='),
            ('2EJkUEP2p2RxXlAKshLgZLwnIAAOhFzNeJhv6wvSxkY=', '2ifGr/4QrabUT9HvzMhYoqTZiZAF1R2kB3d5TU1jLCw='),
            ('3EJkUEP2p2RxXlAKshLgZLwnIAAOhFzNeJhv6wvSxkY=', '3ifGr/4QrabUT9HvzMhYoqTZiZAF1R2kB3d5TU1jLCw='),
        )
        when(self._wireguard_key_generator).generate_psk().thenReturn(
            'pBdexuVBGPb1Rrqk/DfH9JTp95V+li7WwTKigmiuCQc=',
            'q6RUpn6g/iz+c4rQsixD+UMa39n+8NxaVJ70ykYlgkA=',
            'icpKkwUPFcduokh8jTU4iAoLDwCrk9Z+TCAHckmYXH8=',
        )

        self._event_dispatcher = mock()
        when(self._event_dispatcher).dispatch([HQEvent.FEDERATION_BEFORE_CREATE], ...).thenReturn(
            Federation(name='test')
        )
        when(self._event_dispatcher).dispatch([HQEvent.INTERFACES_CONFIGURATION_BEFORE_CREATE], ...).thenReturn(
            wireguard_configuration_valid1()
        )
        when(self._event_dispatcher).dispatch([HQEvent.FORUMS_CONFIGURATION_BEFORE_CREATE], ...).thenReturn(
            wireguard_configuration_valid2()
        )
        when(self._event_dispatcher).dispatch([HQEvent.PHONE_LINES_CONFIGURATION_BEFORE_CREATE], ...).thenReturn(
            wireguard_configuration_valid3()
        )
        when(self._event_dispatcher).dispatch([HQEvent.STATE_BEFORE_CREATE], ...).thenReturn(
            hq_state()
        )
        when(self._event_dispatcher).dispatch([HQEvent.STATE_BEFORE_UPDATE], ...).thenReturn(
            hq_state()
        )
        when(self._event_dispatcher).dispatch([HQEvent.STATE_BEFORE_UPDATE], ...).thenReturn(
            hq_state()
        )
        when(self._event_dispatcher).dispatch([HQEvent.STATE_LOADED], ...).thenReturn(
            hq_state()
        )

        self._logger = mock()

        self._subject = StateDataManager(
            configuration_location_finder=self._configuration_location_finder,
            configuration_loader=self._configuration_loader,
            configuration_saver=self._configuration_saver,
            configuration_locker=self._configuration_locker,
            wireguard_key_generator=self._wireguard_key_generator,
            event_dispatcher=self._event_dispatcher,
            logger=self._logger,
            user_input=self._user_input,
        )

    def test_init(self):
        """ it can be instantiated """

        assert isinstance(self._subject, StateDataManager)

    def test_create_hq_state(self):
        """ it creates and saves a new HQState """

        self._subject.create_hq_state()

        verify(self._configuration_saver, times=1).save(ANY(dict), self._file)
        verify(self._event_dispatcher, times=1).dispatch([HQEvent.STATE_CREATED], ANY(HQState))

    def test_create_hq_state2(self):
        """ it raises a warning if the method for private key retrieval is insecure """

        self._user_input = mock({
            'private_key_retrieval_method': SecretRetrievalMethod.TEST_INSECURE_CLEARTEXT,
            'root_passphrase_command': 'example command',
        })
        self._subject = StateDataManager(
            configuration_location_finder=self._configuration_location_finder,
            configuration_loader=self._configuration_loader,
            configuration_saver=self._configuration_saver,
            configuration_locker=self._configuration_locker,
            wireguard_key_generator=self._wireguard_key_generator,
            event_dispatcher=self._event_dispatcher,
            logger=self._logger,
            user_input=self._user_input,
        )

        self._subject.create_hq_state()

        verify(self._configuration_saver, times=1).save(ANY(dict), self._file)
        verify(self._event_dispatcher, times=1).dispatch([HQEvent.STATE_CREATED], ANY(HQState))
        verify(self._logger, times=1).warning(contains('The root passphrase retrieval method has been set to'))

    def test_create_hq_state3(self):
        """ it creates and saves a new HQState with WG_FEDERATION_ENV_VAR_OR_FILE method for secret retrieval """

        self._user_input = mock({
            'private_key_retrieval_method': SecretRetrievalMethod.WG_FEDERATION_ENV_VAR_OR_FILE,
            'root_passphrase_command': 'example command',
        })
        self._subject = StateDataManager(
            configuration_location_finder=self._configuration_location_finder,
            configuration_loader=self._configuration_loader,
            configuration_saver=self._configuration_saver,
            configuration_locker=self._configuration_locker,
            wireguard_key_generator=self._wireguard_key_generator,
            event_dispatcher=self._event_dispatcher,
            logger=self._logger,
            user_input=self._user_input,
        )

        self._subject.create_hq_state()

        verify(self._configuration_saver, times=1).save(ANY(dict), self._file)
        verify(self._event_dispatcher, times=1).dispatch([HQEvent.STATE_CREATED], ANY(HQState))
        verify(self._logger, times=0).warning(...)

    def test_reload(self):
        """ it reloads the state from default source """

        self._subject.reload()

        verify(self._configuration_loader, times=1).load_if_exists(self._file)
        verify(self._event_dispatcher, times=1).dispatch([HQEvent.STATE_LOADED], ANY(HQState))

    def test_reload2(self):
        """ it raises an error when trying to reload a state that was not bootstrapped """
        when(self._configuration_locker).lock_shared('state').thenRaise(FileNotFoundError)

        with pytest.raises(StateNotBootstrapped) as error:
            self._subject.reload()

        assert 'Unable to load the state: it was not bootstrapped. Run `hq boostrap`.' in str(error.value)

    def test_update_hq_state(self):
        """ it updates a HQState """
        self._subject.update_hq_state({
            'forums': {
                'forum0': {
                    'path': 'test_update'
                }
            }
        })

        verify(self._event_dispatcher, times=1).dispatch([HQEvent.STATE_LOADED], ANY(HQState))
        verify(self._event_dispatcher, times=1).dispatch([HQEvent.STATE_BEFORE_UPDATE], ANY(HQState))
        verify(self._event_dispatcher, times=1).dispatch([HQEvent.STATE_UPDATED], ANY(HQState))
        verify(self._configuration_saver, times=1).save(ANY(dict), self._file)

    def test_update_wireguard_interface_configuration(self):
        """ it updates any WireguardInterface """
        configuration = wireguard_configuration_valid1()
        self._subject.update_wireguard_interface_configuration({
            'path': 'test_update'
        }, configuration)

        verify(self._event_dispatcher, times=1).dispatch([HQEvent.INTERFACES_CONFIGURATION_BEFORE_UPDATE], arg_that(
            lambda arg: {
                isinstance(arg, WireguardConfiguration) and arg.path == 'test_update'
            }
        ))
        verify(self._event_dispatcher, times=1).dispatch([HQEvent.STATE_LOADED], ANY(HQState))
        verify(self._event_dispatcher, times=1).dispatch([HQEvent.STATE_BEFORE_UPDATE], ANY(HQState))
        verify(self._event_dispatcher, times=1).dispatch([HQEvent.STATE_UPDATED], ANY(HQState))
        verify(self._configuration_saver, times=1).save(ANY(dict), self._file)
        verify(self._event_dispatcher, times=1).dispatch([HQEvent.INTERFACES_CONFIGURATION_UPDATED], arg_that(
            lambda arg: {
                isinstance(arg, WireguardConfiguration) and arg.path == 'test_update'
            }
        ))
