import datetime
import logging
import subprocess
from threading import Event
from types import ModuleType

import pytest
from mockito import unstub, mock, verify, arg_that, kwargs, when

from unit.wg_federation import wireguard_configuration_valid3
from wg_federation.concurrent_worker.implementation.wireguard_interface_reload_worker import \
    WireguardInterfaceReloadWorker
from wg_federation.concurrent_worker.worker import Worker
from wg_federation.data.state.wireguard_configuration import WireguardConfiguration
from wg_federation.state.manager.state_data_manager import StateDataManager


class TestWireguardInterfaceReloadWorker:
    """ Test WireguardInterfaceReloadWorker class """

    _hashlib: ModuleType = None
    _state_data_manager: StateDataManager = None
    _logger: logging.Logger = None
    _subprocess: ModuleType = None

    _wait_date: datetime.datetime = None
    _wait_for_event: Event = None
    _data: WireguardConfiguration = None
    _subject: Worker = None

    @pytest.fixture(autouse=True)
    def run_around_tests(self):
        """ Resets mock between tests """
        unstub()
        self.init()

    def init(self):
        """ Constructor """

        self._hashlib = mock()
        self._state_data_manager = mock()
        self._logger = mock()
        self._subprocess = mock()
        self._wait_for_event = Event()
        self._wait_date = datetime.datetime.now()
        self._data = wireguard_configuration_valid3()

        self._subject = WireguardInterfaceReloadWorker(
            hashlib=self._hashlib,
            state_data_manager=self._state_data_manager,
            logger=self._logger,
            subprocess_lib=self._subprocess,
            name='worker',
            context_data=self._data,
        )

    def test_init(self):
        """ it can be instantiated """
        assert isinstance(self._subject, Worker)
        assert isinstance(self._subject, WireguardInterfaceReloadWorker)

    def test_run1(self):
        """ it stops and start wireguard interface and update the data """
        self._subject.run()

        verify(self._state_data_manager, times=1).update_wireguard_interface_configuration(
            arg_that(lambda arg: {
                arg.get('last_loaded_hash') == '0692dec3242410e45f215dcd15e56a7be9220ee601a1d1e9a55bf94dc112f4c5' and
                isinstance(arg.get('last_loaded_date'), datetime.datetime)
            }), self._data)

        verify(self._subprocess, times=1).run([
            '/usr/bin/wg-quick', 'down', '/run/user/1000/wg-federation/phone_lines/phone_lines0.conf'
        ], **kwargs)

        verify(self._subprocess, times=1).run([
            '/usr/bin/wg-quick', 'up', '/run/user/1000/wg-federation/phone_lines/phone_lines0.conf'
        ], **kwargs)

    def test_run2(self):
        """ it silences script error when bringing down the interface fails, but raise any other error """
        when(self._subprocess).run([
            '/usr/bin/wg-quick', 'down', '/run/user/1000/wg-federation/phone_lines/phone_lines0.conf'
        ], **kwargs).thenRaise(subprocess.CalledProcessError(1, 'fails'))

        self._subject.run()

        when(self._subprocess).run([
            '/usr/bin/wg-quick', 'down', '/run/user/1000/wg-federation/phone_lines/phone_lines0.conf'
        ], **kwargs).thenRaise(Exception('this raises'))

        with pytest.raises(Exception):
            self._subject.run()
