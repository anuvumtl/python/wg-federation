from datetime import datetime
from threading import Event

import pytest
from mockito import unstub
from pydantic import BaseModel

from wg_federation.concurrent_worker.worker import Worker


class DummyData(BaseModel):
    """ Dummy data for tests """
    value: str = 'default'


class DummyWorker(Worker[DummyData]):
    """ Dummy Worker for tests """

    _run_wait: Event = None

    # pylint: disable=too-many-arguments

    def __init__(
            self,
            name: str,
            run_wait: Event = None,
            context_data: DummyData = None,
            wait_seconds: int = 0,
            wait_date: datetime = None,
            wait_for: Event = None,
            wait_for_timeout: int = 60
    ) -> None:
        super().__init__(name, context_data, wait_seconds, wait_date, wait_for, wait_for_timeout)
        self._run_wait = run_wait

    def run(self) -> None:
        if self._run_wait:
            self._run_wait.wait()


class TestWorker:
    """ Test Worker class """

    _wait_for_event: Event = None
    _wait_date: datetime = None
    _subject: Worker = None

    @pytest.fixture(autouse=True)
    def run_around_tests(self):
        """ Resets mock between tests """
        unstub()
        self.init()

    def init(self):
        """ Constructor """

        self._wait_for_event = Event()
        self._wait_date = datetime.now()

        self._subject = DummyWorker(
            name='dummy',
            context_data=DummyData(),
            wait_seconds=15,
            wait_date=self._wait_date,
            wait_for=self._wait_for_event,
            wait_for_timeout=1,
        )

    def test_init(self):
        """ it can be instantiated """
        assert isinstance(self._subject, Worker)

    def test_pre_register(self):
        """ it can run some code before registering, doing nothing by default """
        assert not self._subject.pre_register()

    def test_run(self):
        """ it runs """
        assert not self._subject.run()

    def test_get_name(self):
        """ it returns its name """
        assert 'dummy' == self._subject.get_name()

    def test_wait_seconds(self):
        """ it returns the amount of time it should wait in seconds """
        assert 15 == self._subject.wait_seconds()
        assert 0 == DummyWorker(name='dummy2').wait_seconds()

    def test_wait_date(self):
        """ it returns the date it should wait for before running """
        assert self._wait_date == self._subject.wait_date()
        assert not DummyWorker(name='dummy2').wait_date()

    def test_wait_for(self):
        """ it returns the Event it should wait for before running """
        assert self._wait_for_event == self._subject.wait_for()
        assert not DummyWorker(name='dummy2').wait_for()

    def test_wait_for_timeout(self):
        """ it returns the timeout for the Event it should wait for before running """
        assert 1 == self._subject.wait_for_timeout()
        assert 60 == DummyWorker(name='dummy2').wait_for_timeout()
