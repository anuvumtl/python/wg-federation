import datetime
import logging
from threading import Event

import pytest
from mockito import unstub, mock, verify, ANY

from unit.wg_federation.concurrent_worker.test_worker import DummyWorker
from wg_federation.concurrent_worker.worker_container import WorkerContainer
from wg_federation.concurrent_worker.worker_thread import WorkerThread


class TestWorkerContainer:
    """ Test WorkerContainer class """

    _threads: dict[str, WorkerThread] = None
    _logger: logging.Logger = None

    _subject: WorkerContainer = None

    @pytest.fixture(autouse=True)
    def run_around_tests(self):
        """ Resets mock between tests """
        unstub()
        self.init()

    def init(self):
        """ Constructor """

        self._logger = mock()

        self._subject = WorkerContainer(
            logger=self._logger,
        )

    def test_init(self):
        """ it can be instantiated """
        assert isinstance(self._subject, WorkerContainer)

    def test_register1(self):
        """ it registers a Worker """
        self._subject.register(DummyWorker(name='test'))

    def test_register2(self):
        """ it overrides a Worker with the same name that is not running """
        self._subject.register(DummyWorker(name='test'))
        self._subject.register(DummyWorker(name='test'))

        verify(self._logger, times=0).warning(ANY)
        verify(self._logger, times=0).debug(ANY)

    def test_register3(self):
        """ it overrides a Worker with the same name that is currently waiting """
        self._subject.register(DummyWorker(name='test', wait_seconds=10))
        self._subject.start_all()
        self._subject.register(DummyWorker(name='test'))

        verify(self._logger, times=0).warning(ANY)
        verify(self._logger, times=1).debug(
            'Canceling the previously registered Worker “test” as it is not yet running.'
        )

    def test_register4(self):
        """ it fails to overrides a Worker that is currently running """

        run_wait = Event()
        self._subject.register(DummyWorker(name='test', run_wait=run_wait))
        self._subject.start_all()
        self._subject.register(DummyWorker(name='test'))

        # Release the first 'DummyWorker'
        run_wait.set()

        verify(self._logger, times=1).warning('Did not register the Worker “test” as it is currently running.')
        verify(self._logger, times=0).debug(ANY)

    def test_register5(self):
        """ it determines when how much time it should wait before running a Worker """
        self._subject.register(DummyWorker(name='test', wait_date=datetime.datetime.now() + datetime.timedelta(0, 5)))

    def test_start_all1(self):
        """ it starts all the Workers """
        self._subject.register(DummyWorker(name='test'))
        self._subject.register(DummyWorker(name='test2'))
        self._subject.start_all()

    def test_start_all2(self):
        """ it starts all the Workers and de-register them """
        self._subject.register(DummyWorker(name='test'))
        self._subject.register(DummyWorker(name='test2'))
        self._subject.start_all()
        self._subject.start_all()
        verify(self._logger, times=1).debug('test has been run. De-registering it.')
        verify(self._logger, times=1).debug('test2 has been run. De-registering it.')

    def test_start_all3(self):
        """ it starts all the Workers, except the ones already waiting """
        run_wait = Event()
        self._subject.register(DummyWorker(name='test', run_wait=run_wait))
        self._subject.register(DummyWorker(name='test2'))
        self._subject.start_all()
        self._subject.start_all()
        verify(self._logger, times=1).debug('test2 has been run. De-registering it.')
        verify(self._logger, times=1).debug('test was queued to run again, but it is already waiting before running.')
        # Release the first 'DummyWorker'
        run_wait.set()

    def test_join_all1(self):
        """ it blocks the main process until all the Workers are finished running """
        self._subject.register(DummyWorker(name='test'))
        self._subject.register(DummyWorker(name='test2'))
        self._subject.start_all()
        self._subject.join_all()
