from threading import Event
from typing import Callable

import pytest
from mockito import unstub

from wg_federation.concurrent_worker.worker_thread import WorkerThread


class TestWorkerThread:
    """ Test WorkerThread class """

    _function: Callable = None
    _subject: WorkerThread = None

    @pytest.fixture(autouse=True)
    def run_around_tests(self):
        """ Resets mock between tests """
        unstub()
        self.init()

    def init(self):
        """ Constructor """

        self._function = lambda x: x

        self._subject = WorkerThread(
            interval=0,
            function=self._function,
            args=['value'],
        )

    def test_init(self):
        """ it can be instantiated """
        assert isinstance(self._subject, WorkerThread)

    def test_run1(self):
        """ it runs """
        self._subject.run()

    def test_run2(self):
        """ it runs with a blocking event """
        blocking_event = Event()
        # Set immediately for testing purpose. In real life, another thread might set it.
        blocking_event.set()
        WorkerThread(
            interval=0,
            function=self._function,
            args=['value'],
            blocking_event=blocking_event,
        ).run()
