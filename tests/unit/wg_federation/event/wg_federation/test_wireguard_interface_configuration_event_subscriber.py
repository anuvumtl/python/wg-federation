from types import ModuleType

import pytest
from dependency_injector.providers import Factory
from mockito import unstub, mock, verify, ANY

from unit.wg_federation import wireguard_configuration_valid2_not_loaded, wireguard_configuration_valid1
from wg_federation.concurrent_worker.worker_container import WorkerContainer
from wg_federation.data.input.user_input import UserInput
from wg_federation.data.state.wireguard_configuration import WireguardConfiguration
from wg_federation.event.wg_configuration.wg_configuration_event import WGConfigurationEvent
from wg_federation.event.wg_configuration.wireguard_interface_system_reload_event_subscriber import \
    WireguardInterfaceSystemReloadEventSubscriber
from wg_federation.observer.event_subscriber import EventSubscriber


class TestWireguardInterfaceSystemReloadEventSubscriber:
    """ Test WireguardInterfaceSystemReloadEventSubscriber class """

    _user_input: UserInput = None
    _delayed_tasks: WorkerContainer = None
    _wireguard_interface_reload_worker_factory: Factory = None
    _hashlib: ModuleType = None

    _wireguard_configuration: WireguardConfiguration = None
    _subject: WireguardInterfaceSystemReloadEventSubscriber = None

    @pytest.fixture(autouse=True)
    def run_around_tests(self):
        """ Resets mock between tests """
        unstub()
        self.init()

    def init(self):
        """ Constructor """

        self._user_input = mock({'arg1': 'run', 'wg_interface_restart': True})
        self._delayed_tasks = mock()
        self._wireguard_interface_reload_worker_factory = mock()
        self._hashlib = mock()

        self._wireguard_configuration = wireguard_configuration_valid2_not_loaded()

        self._subject = WireguardInterfaceSystemReloadEventSubscriber(
            user_input=self._user_input,
            delayed_tasks=self._delayed_tasks,
            wireguard_interface_reload_worker_factory=self._wireguard_interface_reload_worker_factory,
            hashlib=self._hashlib,
        )

    def test_init(self):
        """ it can be instantiated """
        assert isinstance(self._subject, EventSubscriber)
        assert isinstance(self._subject, WireguardInterfaceSystemReloadEventSubscriber)

    def test_get_subscribed_events(self):
        """ it returns subscribed events """
        assert WGConfigurationEvent.CONFIGURATION_FILE_CREATED in self._subject.get_subscribed_events()
        assert WGConfigurationEvent.CONFIGURATION_FILE_UPDATED in self._subject.get_subscribed_events()

    def test_get_order(self):
        """ it returns its order of execution """
        assert 500 == self._subject.get_order()

    def test_must_stop_propagation(self):
        """ it returns whether or not it should stop propagation """
        assert not self._subject.must_stop_propagation()

    def test_should_run1(self):
        """ it only runs if all the conditions are met """
        assert self._subject.should_run(self._wireguard_configuration)

    def test_should_run2(self):
        """ it does not run if the loaded hash does not correspond to the current content """
        assert not self._subject.should_run(wireguard_configuration_valid1())

    def test_should_run3(self):
        """ it does not run if the command argument is not “run” """
        self._user_input = mock({'arg1': 'not', 'wg_interface_restart': False})
        self._subject = WireguardInterfaceSystemReloadEventSubscriber(
            user_input=self._user_input,
            delayed_tasks=self._delayed_tasks,
            wireguard_interface_reload_worker_factory=self._wireguard_interface_reload_worker_factory,
            hashlib=self._hashlib,
        )
        assert not self._subject.should_run(self._wireguard_configuration)

    def test_should_run4(self):
        """ it does run if the command argument is not “run” but the flag “wg_interface_restart” is set """
        self._user_input = mock({'arg1': 'not', 'wg_interface_restart': True})
        self._subject = WireguardInterfaceSystemReloadEventSubscriber(
            user_input=self._user_input,
            delayed_tasks=self._delayed_tasks,
            wireguard_interface_reload_worker_factory=self._wireguard_interface_reload_worker_factory,
            hashlib=self._hashlib,
        )
        assert self._subject.should_run(self._wireguard_configuration)

    def test_run1(self):
        """ it creates a Worker and add it to the delayed WorkerPool """
        assert self._wireguard_configuration == self._subject.run(self._wireguard_configuration)
        verify(self._delayed_tasks, times=1).register(ANY)
