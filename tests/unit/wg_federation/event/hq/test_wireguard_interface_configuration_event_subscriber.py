from types import ModuleType

import pytest
from mockito import unstub, mock, when, ANY, verify, patch

from unit.wg_federation import wireguard_configuration_valid3, wireguard_configuration_valid3_loaded
from wg_federation.data.state.wireguard_configuration import WireguardConfiguration
from wg_federation.data_transformation.configuration_location_finder import ConfigurationLocationFinder
from wg_federation.data_transformation.locker.configuration_locker import ConfigurationLocker
from wg_federation.event.hq.hq_event import HQEvent
from wg_federation.event.hq.wireguard_interface_configuration_event_subscriber import \
    WireguardInterfaceConfigurationEventSubscriber
from wg_federation.event.wg_configuration.wg_configuration_event import WGConfigurationEvent
from wg_federation.observer.event_dispatcher import EventDispatcher
from wg_federation.observer.event_subscriber import EventSubscriber
from wg_federation.utils.utils import Utils


class TestWireguardConfigurationEventSubscriber:
    """ Test WireguardConfigurationEventSubscriber class """

    _os_path = None
    _file = None
    _lock = None
    _wireguard_configuration: WireguardConfiguration = None

    _os_lib: ModuleType = None
    _configuration_location_finder: ConfigurationLocationFinder = None
    _configuration_locker: ConfigurationLocker = None
    _event_dispatcher: EventDispatcher = None
    _subject: WireguardInterfaceConfigurationEventSubscriber = None

    @pytest.fixture(autouse=True)
    def run_around_tests(self):
        """ Resets mock between tests """
        unstub()
        self.init()

    def init(self):
        """ Constructor """
        self._wireguard_configuration = wireguard_configuration_valid3()

        self._file = mock()

        self._lock = mock()
        # pylint: disable=unnecessary-dunder-call
        when(self._lock).__enter__(...).thenReturn(self._file)
        when(self._lock).__exit__(...).thenReturn(self._file)

        self._os_path = mock()
        when(self._os_path).join('forum_dir', ANY).thenReturn('forum_fullpath')
        when(self._os_path).join('interface_dir', ANY).thenReturn('interface_fullpath')
        when(self._os_path).join('phone_line_dir', ANY).thenReturn('phone_line_fullpath')

        self._configuration_locker = mock()
        when(self._configuration_locker).lock_exclusively(ANY).thenReturn(self._lock)

        self._event_dispatcher = mock()

        self._os_lib = mock({'path': self._os_path})

        self._subject = WireguardInterfaceConfigurationEventSubscriber(
            os_lib=self._os_lib,
            configuration_locker=self._configuration_locker,
            event_dispatcher=self._event_dispatcher,
        )

    def test_init(self):
        """ it can be instantiated """
        assert isinstance(self._subject, EventSubscriber)
        assert isinstance(self._subject, WireguardInterfaceConfigurationEventSubscriber)

    def test_get_subscribed_events(self):
        """ it returns subscribed events """
        assert HQEvent.INTERFACES_CONFIGURATION_BEFORE_CREATE in self._subject.get_subscribed_events()
        assert HQEvent.FORUMS_CONFIGURATION_BEFORE_CREATE in self._subject.get_subscribed_events()
        assert HQEvent.PHONE_LINES_CONFIGURATION_BEFORE_CREATE in self._subject.get_subscribed_events()
        assert HQEvent.INTERFACES_CONFIGURATION_BEFORE_UPDATE in self._subject.get_subscribed_events()
        assert HQEvent.FORUMS_CONFIGURATION_BEFORE_UPDATE in self._subject.get_subscribed_events()
        assert HQEvent.PHONE_LINES_CONFIGURATION_BEFORE_UPDATE in self._subject.get_subscribed_events()

    def test_get_order(self):
        """ it returns its order of execution """
        assert 500 == self._subject.get_order()

    def test_must_stop_propagation(self):
        """ it returns whether or not it should stop propagation """
        assert not self._subject.must_stop_propagation()

    def test_should_run(self):
        """ it only runs when the WireguardConfiguration data hash differs from the one already saved on system """
        assert self._subject.should_run(self._wireguard_configuration)

        assert not self._subject.should_run(wireguard_configuration_valid3_loaded())

    def test_run1(self):
        """ it creates all wireguard configuration files for the given interface """
        patch(Utils.open, lambda x, y, z: self._lock)

        assert self._wireguard_configuration == self._subject.run(self._wireguard_configuration)

        verify(self._configuration_locker, times=1).lock_exclusively(
            '/run/user/1000/wg-federation/phone_lines/phone_lines0.conf'
        )
        verify(self._event_dispatcher, times=1).dispatch(
            [WGConfigurationEvent.CONFIGURATION_FILE_BEFORE_CREATE], self._wireguard_configuration
        )
        verify(self._event_dispatcher, times=1).dispatch(
            [WGConfigurationEvent.CONFIGURATION_FILE_CREATED], self._wireguard_configuration
        )

        verify(self._event_dispatcher, times=0).dispatch([WGConfigurationEvent.CONFIGURATION_FILE_BEFORE_UPDATE], ANY)
        verify(self._event_dispatcher, times=0).dispatch([WGConfigurationEvent.CONFIGURATION_FILE_UPDATED], ANY)

    def test_run2(self):
        """ it dispatches UPDATE events when the WireGuard configuration file already exists """
        patch(Utils.open, lambda x, y, z: self._lock)

        data = wireguard_configuration_valid3_loaded()

        assert data == self._subject.run(data)

        verify(self._event_dispatcher, times=0).dispatch([WGConfigurationEvent.CONFIGURATION_FILE_BEFORE_CREATE], ANY)
        verify(self._event_dispatcher, times=0).dispatch([WGConfigurationEvent.CONFIGURATION_FILE_CREATED], ANY)

        verify(self._event_dispatcher, times=1).dispatch([WGConfigurationEvent.CONFIGURATION_FILE_BEFORE_UPDATE], data)
        verify(self._event_dispatcher, times=1).dispatch([WGConfigurationEvent.CONFIGURATION_FILE_UPDATED], data)
